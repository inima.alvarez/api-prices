--delete all data from prices
delete from prices;


--populate price entity
insert into prices (id, brand_id, start_date, end_date, price_list, product_id, priority, price, curr, last_date)
values  (nextval('"prices_seq"'), 1, '2020-06-14 00:00:00', '2020-12-31 23:59:59', 1, 35455 , 0, 35.50, 'USD', now());
insert into prices (id, brand_id, start_date, end_date, price_list, product_id, priority, price, curr, last_date)
values  (nextval('"prices_seq"'), 1, '2020-06-14 15:00:00', '2020-06-14 18:30:00', 2, 35455 , 1, 25.45, 'USD', now());
insert into prices (id, brand_id, start_date, end_date, price_list, product_id, priority, price, curr, last_date)
values  (nextval('"prices_seq"'), 1, '2020-06-15 00:00:00', '2020-06-15 11:00:00', 3, 35455 , 1, 30.50, 'USD', now());
insert into prices (id, brand_id, start_date, end_date, price_list, product_id, priority, price, curr, last_date)
values  (nextval('"prices_seq"'), 1, '2020-06-15 16:00:00', '2020-12-31 23:59:59', 4, 35455 , 1, 38.95, 'USD', now());