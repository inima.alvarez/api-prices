CREATE TABLE taxes
(
    id     int4 NULL,
    "name" varchar NULL,
    CONSTRAINT taxes_pk PRIMARY KEY (id)
);

CREATE SEQUENCE public.taxes_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;