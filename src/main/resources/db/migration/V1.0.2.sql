CREATE TABLE brand (
                       id int4 NOT NULL,
                       "name" varchar NOT NULL,
                       last_date timestamp NULL DEFAULT now(),
                       CONSTRAINT brand_pk PRIMARY KEY (id)
);