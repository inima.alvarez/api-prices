INSERT INTO taxes (id, "name") VALUES(nextval('"taxes_seq"'), 'TAXES FOR PRODUCT 1');
INSERT INTO taxes (id, "name") VALUES(nextval('"taxes_seq"'), 'TAXES FOR PRODUCT 2');
INSERT INTO taxes (id, "name") VALUES(nextval('"taxes_seq"'), 'TAXES FOR PRODUCT 3');
INSERT INTO taxes (id, "name") VALUES(nextval('"taxes_seq"'), 'TAXES FOR PRODUCT 4');

ALTER TABLE prices ADD CONSTRAINT prices_fk_prices_list FOREIGN KEY (price_list) REFERENCES taxes(id);