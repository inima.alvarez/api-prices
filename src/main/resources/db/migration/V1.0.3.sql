CREATE TABLE prices (
                        id int4 NOT NULL,
                        brand_id int4 NOT NULL,
                        start_date timestamp NOT NULL,
                        end_date timestamp NOT NULL,
                        price_list int4 NOT NULL,
                        product_id int4 NOT NULL,
                        priority int4 NOT NULL,
                        price float8 NOT NULL,
                        curr varchar NOT NULL,
                        last_date timestamp NULL DEFAULT now(),
                        CONSTRAINT prices_pk PRIMARY KEY (id),
                        CONSTRAINT prices_fk FOREIGN KEY (brand_id) REFERENCES brand(id),
                        CONSTRAINT prices_fk_1 FOREIGN KEY (product_id) REFERENCES product(id)
);