create or replace procedure populate_user_role(inout rows_p bigint default 0)
language plpgsql
as $procedure$
begin
	declare
rec_r record;
	 rec_u record;
	 cur_user refcursor;
     cur_rol refcursor;

begin
open cur_user for select u.* from "user" u for read only;
loop

fetch cur_user into rec_u;

            exit when not found;

open cur_rol for select r.* from rol r for read only;
loop

fetch cur_rol into rec_r;

                exit when not found;

insert into user_rol (id, "user", rol) VALUES (nextval('"user_rol_seq"'), rec_u.id, rec_r.id);

end loop;
close cur_rol;
end loop;
close cur_user;
rows_p:=1;
end;
end;
$procedure$
;

call populate_user_role(0);

drop procedure if exists populate_user_role;