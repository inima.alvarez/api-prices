CREATE TABLE "user"
(
    id         int4    NOT NULL,
    username   varchar NOT NULL,
    "password" varchar NOT NULL,
    CONSTRAINT user_pk PRIMARY KEY (id)
);

CREATE SEQUENCE user_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;

CREATE TABLE "rol"
(
    id     int4 NOT NULL,
    "name" varchar NULL,
    CONSTRAINT role_pk PRIMARY KEY (id)
);

CREATE SEQUENCE rol_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;

CREATE TABLE user_rol
(
    "user" int4 NOT NULL,
    rol    int4 NULL,
    id     int4 NOT NULL,
    CONSTRAINT user_rol_pk PRIMARY KEY (id),
    CONSTRAINT user_role_fk FOREIGN KEY ("user") REFERENCES public."user" (id),
    CONSTRAINT user_role_fk_1 FOREIGN KEY (rol) REFERENCES public.rol (id)
);
CREATE SEQUENCE user_rol_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;

INSERT INTO public."user" (id, username, "password") VALUES(nextval('"user_seq"'), 'user', 'userpass');
INSERT INTO public."user" (id, username, "password") VALUES(nextval('"user_seq"'), 'admin', 'root');

INSERT INTO rol (id, "name") VALUES(nextval('"rol_seq"'), 'USER');
INSERT INTO rol (id, "name") VALUES(nextval('"rol_seq"'), 'ADMIN');

