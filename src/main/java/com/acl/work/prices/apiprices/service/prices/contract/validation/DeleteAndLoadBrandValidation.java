package com.acl.work.prices.apiprices.service.prices.contract.validation;

import com.acl.work.prices.apiprices.core.exceptions.CustomNotFoundException;
import com.acl.work.prices.apiprices.core.facade.error.ErrorCode;
import com.acl.work.prices.apiprices.service.prices.datasource.BrandRepository;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static com.acl.work.prices.apiprices.util.msg.MessageContextHolder.msg;

@Aspect
@Component
public class DeleteAndLoadBrandValidation {
    private final static String DEFAULT_ARGS = "joinPoint, id";
    private final Logger logger = LoggerFactory.getLogger(DeleteAndLoadBrandValidation.class);
    private final BrandRepository brandRepository;

    public DeleteAndLoadBrandValidation(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }

    @Before(value = "execution(* com.acl.work.prices.apiprices.service.prices.BrandService+.get(..)) && args(id) " +
            "|| execution(* com.acl.work.prices.apiprices.service.prices.BrandService+.delete(..)) && args(id)", argNames = DEFAULT_ARGS)
    public void beforeAdvice(JoinPoint joinPoint, Integer id) throws CustomNotFoundException {
        this.brandRepository.findById((long) id)
                .orElseThrow(() -> {
                    logger.error(msg("app.message.brand.not.found", id));
                    throw new CustomNotFoundException(ErrorCode.NOT_FOUND, msg("app.message.brand.not.found", id));
                });
    }
}
