package com.acl.work.prices.apiprices.core.security;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class WebMvcSecurityConfig implements WebMvcConfigurer {
    private final JwtTokenProvider jwtTokenProvider;

    public WebMvcSecurityConfig(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new HttpSecurityInterceptor(jwtTokenProvider));
    }
}
