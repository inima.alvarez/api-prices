package com.acl.work.prices.apiprices.facade;

import com.acl.work.prices.apiprices.core.facade.Result;
import com.acl.work.prices.apiprices.service.prices.contract.to.BrandCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.BrandTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;

import java.util.List;

/**
 * Default interface for {@link BrandFacade}
 * Orchestrate operations over brand service interface
 *
 * @author inima
 * @since 0.1.0
 */
public interface BrandFacade {
    Result<BrandTO> create(BrandTO to);

    Result<BrandTO> load(Integer id);

    Result<BrandTO> update(BrandTO to);

    Result<BrandTO> delete(Integer id);

    Result<PageList<BrandTO>> search(BrandCriteriaTO criteria);
}
