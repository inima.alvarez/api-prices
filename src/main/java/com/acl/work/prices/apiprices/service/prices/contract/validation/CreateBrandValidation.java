package com.acl.work.prices.apiprices.service.prices.contract.validation;

import com.acl.work.prices.apiprices.core.exceptions.BusinessException;
import com.acl.work.prices.apiprices.service.prices.contract.to.BrandTO;
import com.acl.work.prices.apiprices.service.prices.datasource.BrandRepository;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static com.acl.work.prices.apiprices.core.facade.error.ErrorCode.BAD_REQUEST;
import static com.acl.work.prices.apiprices.util.msg.MessageContextHolder.msg;

@Aspect
@Component
public class CreateBrandValidation {
    private final static String DEFAULT_ARGS = "joinPoint, to";
    private final Logger logger = LoggerFactory.getLogger(CreateBrandValidation.class);
    private final BrandRepository brandRepository;

    public CreateBrandValidation(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }

    @Before(value = "execution(* com.acl.work.prices.apiprices.service.prices.BrandService+.create(..)) && args(to)", argNames = DEFAULT_ARGS)
    public void beforeAdvice(JoinPoint joinPoint, BrandTO to) throws BusinessException {

        var brand = brandRepository.findByName(to.getName());
        if (Objects.nonNull(brand)) {
            logger.error(msg("app.message.brand.create.found", to.getName()));
            throw new BusinessException(BAD_REQUEST, msg("app.message.brand.create.found", to.getName()));
        }
    }
}
