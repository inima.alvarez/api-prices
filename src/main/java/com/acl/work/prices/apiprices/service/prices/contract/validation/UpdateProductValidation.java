package com.acl.work.prices.apiprices.service.prices.contract.validation;

import com.acl.work.prices.apiprices.core.exceptions.CustomNotFoundException;
import com.acl.work.prices.apiprices.core.facade.error.ErrorCode;
import com.acl.work.prices.apiprices.service.prices.contract.to.ProductTO;
import com.acl.work.prices.apiprices.service.prices.datasource.ProductRepository;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static com.acl.work.prices.apiprices.util.msg.MessageContextHolder.msg;

@Aspect
@Component
public class UpdateProductValidation {
    private final static String DEFAULT_ARGS = "joinPoint, to";
    private final Logger logger = LoggerFactory.getLogger(UpdateProductValidation.class);
    private final ProductRepository productRepository;

    public UpdateProductValidation(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @Before(value = "execution(* com.acl.work.prices.apiprices.service.prices.ProductService+.update(..)) && args(to)", argNames = DEFAULT_ARGS)
    public void beforeAdvice(JoinPoint joinPoint, ProductTO to) throws CustomNotFoundException {
        this.productRepository.findById((long) to.getId())
                .orElseThrow(() -> {
                    logger.error(msg("app.message.product.not.found", to.getId()));
                    throw new CustomNotFoundException(ErrorCode.NOT_FOUND, msg("app.message.product.not.found", to.getId()));
                });
    }
}
