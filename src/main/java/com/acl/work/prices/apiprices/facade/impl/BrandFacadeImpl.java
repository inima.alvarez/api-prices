package com.acl.work.prices.apiprices.facade.impl;

import com.acl.work.prices.apiprices.core.facade.Result;
import com.acl.work.prices.apiprices.core.facade.stereotype.Facade;
import com.acl.work.prices.apiprices.facade.BrandFacade;
import com.acl.work.prices.apiprices.service.prices.BrandService;
import com.acl.work.prices.apiprices.service.prices.contract.to.BrandCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.BrandTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;

import java.util.List;

import static com.acl.work.prices.apiprices.core.facade.Result.successful;

@Facade("brandFacade")
public class BrandFacadeImpl implements BrandFacade {

    private final BrandService brandService;

    public BrandFacadeImpl(BrandService brandService) {
        this.brandService = brandService;
    }

    @Override
    public Result<BrandTO> create(BrandTO to) {
        var result = this.brandService.create(to);
        return successful(result);
    }

    @Override
    public Result<BrandTO> load(Integer id) {
        var result = this.brandService.get(id);
        return successful(result);
    }

    @Override
    public Result<BrandTO> update(BrandTO to) {
        var result = this.brandService.update(to);
        return successful(result);
    }

    @Override
    public Result<BrandTO> delete(Integer id) {
        this.brandService.delete(id);
        return successful();
    }

    @Override
    public Result<PageList<BrandTO>> search(BrandCriteriaTO criteria) {
        var all = this.brandService.loadAll(criteria);
        return successful(all);
    }
}
