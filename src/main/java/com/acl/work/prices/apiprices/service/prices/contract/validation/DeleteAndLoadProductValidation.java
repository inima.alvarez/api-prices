package com.acl.work.prices.apiprices.service.prices.contract.validation;

import com.acl.work.prices.apiprices.core.exceptions.CustomNotFoundException;
import com.acl.work.prices.apiprices.core.facade.error.ErrorCode;
import com.acl.work.prices.apiprices.service.prices.datasource.ProductRepository;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static com.acl.work.prices.apiprices.core.facade.error.ErrorCode.NOT_FOUND;
import static com.acl.work.prices.apiprices.util.msg.MessageContextHolder.msg;

@Aspect
@Component
public class DeleteAndLoadProductValidation {
    private final static String DEFAULT_ARGS = "joinPoint, id";
    private final Logger logger = LoggerFactory.getLogger(DeleteAndLoadProductValidation.class);

    private final ProductRepository productRepository;

    public DeleteAndLoadProductValidation(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @Before(value = "execution(* com.acl.work.prices.apiprices.service.prices.ProductService+.get(..)) && args(id) " +
            "|| execution(* com.acl.work.prices.apiprices.service.prices.ProductService+.delete(..)) && args(id)", argNames = DEFAULT_ARGS)
    public void beforeAdvice(JoinPoint joinPoint, Integer id) throws CustomNotFoundException {
        this.productRepository.findById((long) id)
                .orElseThrow(() -> {
                    logger.error(msg("app.message.product.not.found", id));
                    throw new CustomNotFoundException(NOT_FOUND, msg("app.message.product.not.found", id));
                });
    }
}
