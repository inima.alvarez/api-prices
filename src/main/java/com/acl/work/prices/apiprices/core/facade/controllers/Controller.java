package com.acl.work.prices.apiprices.core.facade.controllers;

import com.acl.work.prices.apiprices.core.facade.Result;
import com.acl.work.prices.apiprices.core.facade.error.Error;
import com.acl.work.prices.apiprices.core.facade.error.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.function.Function;

import static com.acl.work.prices.apiprices.core.facade.error.ErrorCode.FORBIDDEN_ACCESS;
import static com.acl.work.prices.apiprices.core.facade.error.ErrorCode.INVALID_REFERENCE;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

public abstract class Controller<F> {
    private static final ResponseEntity NOT_CONTENT = noContent().build();
    private static final ResponseEntity NOT_FOUND = notFound().build();
    private static final ResponseEntity INTERNAL_ERROR = status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    private static final ResponseEntity FORBIDDEN = status(HttpStatus.FORBIDDEN).build();
    protected @Autowired
    F facade;

    protected <V> ResponseEntity create(Function<F, Result<V>> function) {
        Result<?> result = function.apply(facade);
        return result.isSuccess() ? ok(result.getVal())
                : this.getErrorResponse(result.getErrors());
    }

    protected <V> ResponseEntity perform(Function<F, Result<V>> function) {
        Result<?> result = function.apply(facade);
        return result.isSuccess() ? NOT_CONTENT : this.getErrorResponse(result.getErrors());
    }

    protected <V> ResponseEntity get(Function<F, Result<V>> function) {
        Result<?> result = function.apply(facade);
        return result.isSuccess() ? ok(result.getVal()) : this.getErrorResponse(result.getErrors());
    }

    //<editor-fold desc="Support methods">
    protected ResponseEntity getErrorResponse(List<Error> errors) {
        var code = errors.stream()
                .map(Error::getCode)
                .reduce(ErrorCode.INTERNAL_ERROR, this::fold);
        switch (code) {
            case FORBIDDEN_ACCESS:
                return FORBIDDEN;
            case INVALID_REFERENCE:
                return NOT_FOUND;
            default:
                return code.is1xxValidation()
                        ? badRequest().body(errors)
                        : code.is2xxSecurity()
                        ? status(HttpStatus.FORBIDDEN).body(errors)
                        : code.is3xxBackEnd()
                        ? status(HttpStatus.INTERNAL_SERVER_ERROR).body(errors)
                        : INTERNAL_ERROR;
        }
    }

    private ErrorCode fold(ErrorCode left, ErrorCode right) {
        if (FORBIDDEN_ACCESS == left || FORBIDDEN_ACCESS == right)
            return FORBIDDEN_ACCESS == left ? left : right;

        if (INVALID_REFERENCE == left || INVALID_REFERENCE == right)
            return INVALID_REFERENCE == left ? left : right;

        if (left.is1xxValidation() || right.is1xxValidation())
            return left.is1xxValidation() ? left : right;
        else if (left.is2xxSecurity() || right.is2xxSecurity())
            return left.is2xxSecurity() ? left : right;
        else if (left.is3xxBackEnd() || right.is3xxBackEnd())
            return left.is3xxBackEnd() ? left : right;

        return left;
    }
}
