package com.acl.work.prices.apiprices.service.prices;

import com.acl.work.prices.apiprices.service.prices.contract.to.ProductCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.ProductTO;

import java.util.List;

/**
 * Default interface for {@link ProductService}
 * Perform operations over product repository
 *
 * @author Inima
 * @since 0.1.0
 */
public interface ProductService {
    ProductTO create(ProductTO to);

    ProductTO get(Integer id);

    ProductTO update(ProductTO to);

    void delete(Integer id);

    List<ProductTO> loadAll(ProductCriteriaTO criteria);
}
