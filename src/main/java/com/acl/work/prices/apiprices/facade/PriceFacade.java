package com.acl.work.prices.apiprices.facade;

import com.acl.work.prices.apiprices.core.facade.Result;
import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;
import com.acl.work.prices.apiprices.service.prices.contract.to.PriceCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PriceResponseTO;

import java.util.List;

/**
 * Default interface for {@link PriceFacade}
 * Orchestrate operations over prices service interface
 *
 * @author inima
 * @since 0.1.0
 */
public interface PriceFacade {
    Result<PageList<PriceResponseTO>> search(PriceCriteriaTO criteria);
}
