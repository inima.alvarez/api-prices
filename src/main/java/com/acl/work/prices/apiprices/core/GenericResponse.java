package com.acl.work.prices.apiprices.core;

import com.fasterxml.jackson.annotation.JsonInclude;

public class GenericResponse {
    private Object result;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object payload;

    public GenericResponse() {
        super();
    }

    public GenericResponse(Object result) {
        super();
        this.result = result;
    }

    public GenericResponse(Object payload, String message) {
        super();
        this.payload = payload;
        this.result = new MsgResultResponse(message);
    }

    public Object getResult() {
        return result;
    }

    public Object getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return "GenericDtoResponse [result=" + result + ", payload=" + payload + "]";
    }

    public class MsgResultResponse {

        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String message;

        public MsgResultResponse(String message) {
            super();
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

    }
}
