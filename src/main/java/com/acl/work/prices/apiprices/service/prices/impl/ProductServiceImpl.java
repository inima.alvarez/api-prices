package com.acl.work.prices.apiprices.service.prices.impl;

import com.acl.work.prices.apiprices.core.exceptions.CustomNotFoundException;
import com.acl.work.prices.apiprices.core.facade.error.ErrorCode;
import com.acl.work.prices.apiprices.service.prices.ProductService;
import com.acl.work.prices.apiprices.service.prices.contract.to.ProductCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.ProductTO;
import com.acl.work.prices.apiprices.service.prices.datasource.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static com.acl.work.prices.apiprices.service.prices.contract.ProductBinder.PRODUCT_BINDER;
import static java.lang.String.format;

@Service("productService")
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public ProductTO create(ProductTO to) {
        var entity = PRODUCT_BINDER.bind(to);
        var saved = this.productRepository.save(entity);
        return PRODUCT_BINDER.bind(saved);
    }

    @Override
    public ProductTO get(Integer id) {
        return this.productRepository.findById((long) id)
                .map(PRODUCT_BINDER::bind)
                .orElse(new ProductTO());
    }

    @Override
    public ProductTO update(ProductTO to) {
        return this.productRepository.findById((long) to.getId())
                .map(entity -> PRODUCT_BINDER.bind(entity, to))
                .flatMap(brand -> Optional.of(this.productRepository.save(brand)))
                .map(PRODUCT_BINDER::bind)
                .orElse(new ProductTO());
    }

    @Override
    public void delete(Integer id) {
        var brand = this.productRepository.findById((long) id)
                .orElseThrow(() -> new CustomNotFoundException(ErrorCode.NOT_FOUND));
        this.productRepository.delete(brand);
    }

    @Override
    public List<ProductTO> loadAll(ProductCriteriaTO criteria) {
        var source = criteria.getCriteria();
        var brandList = source
                .equals("*")
                ? this.productRepository.findAll()
                : this.productRepository.findAllByNameLike(criteria.getCriteria());
        return PRODUCT_BINDER.bind(brandList);
    }
}
