package com.acl.work.prices.apiprices.facade;

import com.acl.work.prices.apiprices.core.facade.Result;
import com.acl.work.prices.apiprices.service.security.contract.to.UserRequestTO;
import com.acl.work.prices.apiprices.service.security.contract.to.UserResponseTO;

public interface AuthenticationFacade {
    Result<UserResponseTO> logging(UserRequestTO request);
}
