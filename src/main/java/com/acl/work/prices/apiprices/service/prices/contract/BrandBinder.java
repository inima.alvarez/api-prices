package com.acl.work.prices.apiprices.service.prices.contract;

import com.acl.work.prices.apiprices.service.prices.contract.to.BrandTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;
import com.acl.work.prices.apiprices.service.prices.datasource.domain.Brand;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BrandBinder {
    BrandBinder BRAND_BINDER = Mappers.getMapper(BrandBinder.class);

    Brand bind(BrandTO brand);

    @Mapping(target = "withId", source = "id")
    BrandTO bind(Brand brand);

    List<BrandTO> bindList(List<Brand> source);

   default PageList<BrandTO> bind(List<Brand> source) {
       var target = this.bindList(source);
       return new PageList<BrandTO>()
               .withElements(target);
   }

    @Mapping(target = "id", ignore = true)
    Brand bind(@MappingTarget Brand target, BrandTO source);

    List<Brand> bindBulk(List<BrandTO> source);
}
