package com.acl.work.prices.apiprices.service.security.impl;

import com.acl.work.prices.apiprices.service.security.AuthenticationService;
import com.acl.work.prices.apiprices.service.security.contract.to.UserRequestTO;
import com.acl.work.prices.apiprices.service.security.datasource.UserRepository;
import com.acl.work.prices.apiprices.service.security.datasource.UserRoleRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static com.acl.work.prices.apiprices.service.security.contract.UserBinder.USER_BINDER;
import static java.util.stream.Collectors.toList;

@Service("authenticationService")
public class AuthenticationServiceImpl implements AuthenticationService, UserDetailsService {

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;

    public AuthenticationServiceImpl(UserRepository userRepository, UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public UserDetails logging(UserRequestTO request) {
        return this.loadUserByUsername(request.getUsername());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        var userRoles = this.userRoleRepository.findByName(username);
        var userRol = USER_BINDER.bind(userRoles);

        if (Objects.isNull(userRol.getUser().getId()))
            throw new UsernameNotFoundException("User not found with username: " + username);

        var authorities = userRol
                .getRol()
                .stream()
                .map(g -> (GrantedAuthority) g::getName)
                .collect(toList());

        return new User(userRol.getUser().getUsername(), userRol.getUser().getPassword(), authorities);
    }
}
