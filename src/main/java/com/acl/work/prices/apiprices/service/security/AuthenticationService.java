package com.acl.work.prices.apiprices.service.security;

import com.acl.work.prices.apiprices.service.security.contract.to.UserRequestTO;
import org.springframework.security.core.userdetails.UserDetails;

public interface AuthenticationService {
    UserDetails logging(UserRequestTO request);
}
