package com.acl.work.prices.apiprices.core.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfiguration {
    @Bean
    public OpenAPI springShopOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("PRICE ECOMMERCE MICROSERVICE")
                        .description("API to perform ecommerce operations")
                        .version("1.0.0")
                );
    }
}
