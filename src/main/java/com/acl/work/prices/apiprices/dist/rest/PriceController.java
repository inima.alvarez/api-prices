package com.acl.work.prices.apiprices.dist.rest;

import com.acl.work.prices.apiprices.core.facade.controllers.Controller;
import com.acl.work.prices.apiprices.core.security.stereotypes.Auth;
import com.acl.work.prices.apiprices.facade.PriceFacade;
import com.acl.work.prices.apiprices.service.prices.contract.to.BrandCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;
import com.acl.work.prices.apiprices.service.prices.contract.to.PriceCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PriceResponseTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/private/price/")
@Tag(name = "price", description = "API to manage prices")
public class PriceController extends Controller<PriceFacade> {
    @Operation(summary = "Search prices information and product taxes by given criteria", description = "Name search by %name% format", tags = {"price"})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "successful operation",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = PriceResponse.class))))
    })
    @PostMapping("search")
    @Auth(hasRole = "USER")
    public ResponseEntity search(@RequestBody @Validated PriceCriteriaTO criteria) {
        return this.get(facade -> facade.search(criteria));
    }

    private static class PriceResponse extends PageList<PriceResponseTO>{}
}
