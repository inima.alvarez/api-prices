package com.acl.work.prices.apiprices.service.prices.contract.to;

import com.acl.work.prices.apiprices.core.TransferObject;

public class BrandCriteriaTO extends TransferObject {
    private String criteria;

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }
}
