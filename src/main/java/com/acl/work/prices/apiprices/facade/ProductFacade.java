package com.acl.work.prices.apiprices.facade;

import com.acl.work.prices.apiprices.core.facade.Result;
import com.acl.work.prices.apiprices.service.prices.contract.to.ProductCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.ProductTO;

import java.util.List;

/**
 * Default interface for {@link ProductFacade}
 * Orchestrate operations over product service interface
 *
 * @author inima
 * @since 0.1.0
 */
public interface ProductFacade {
    Result<ProductTO> create(ProductTO to);

    Result<ProductTO> load(Integer id);

    Result<ProductTO> update(ProductTO to);

    Result<ProductTO> delete(Integer id);

    Result<List<ProductTO>> search(ProductCriteriaTO criteria);
}
