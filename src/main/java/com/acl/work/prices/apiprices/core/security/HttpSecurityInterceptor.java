package com.acl.work.prices.apiprices.core.security;

import com.acl.work.prices.apiprices.core.security.stereotypes.Auth;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.acl.work.prices.apiprices.util.msg.MessageContextHolder.msg;


@Component
public class HttpSecurityInterceptor implements HandlerInterceptor {
    private final Logger logger = LoggerFactory.getLogger(HttpSecurityInterceptor.class);
    private final static String[] interceptPaths = new String[]{"", "search"};
    private static final String HEADER_TOKEN = "Authorization";

    private final Predicate<HttpServletRequest> isHealthEndPoint = request -> {
        var paths = request.getPathInfo().split("/");
        var path = paths[paths.length - 1];
        var filter = new String[] {
                "logging",
                "swagger",
                "index",
                "api-docs",
                "icon"
        };
        return Arrays.stream(filter).anyMatch(path::contains);
    };

    private final JwtTokenProvider jwtTokenProvider;

    public HttpSecurityInterceptor(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (this.isHealthEndPoint.test(request)) return true;

        var token = request.getHeader(HEADER_TOKEN);
        if (Objects.isNull(token) || token.trim().equals("")) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            logger.info(msg("app.security.token.retrieve.message"));
            return false;
        }
        return !validateToken(request, response);
    }

    private boolean validateToken(HttpServletRequest request, HttpServletResponse response) {

        if (request.getPathInfo().contains("health")) return false;

        var tokenValue = this.jwtTokenProvider.resolveToken(request);
        if (Objects.isNull(tokenValue)) return true;

        Method[] methods = this.getControllersMethods();

        if (Objects.isNull(methods) || Arrays.asList(methods).isEmpty()) return true;

        for (Method method : methods) {
            if (Objects.isNull(method)) return false;
            Auth annotation = method.getAnnotation(Auth.class);
            if (Objects.nonNull(annotation)) {
                var role = this.jwtTokenProvider.getRoleFromToken(tokenValue);
                return this.validateRole(response, annotation, role);
            }
            return false;
        }

        return true;
    }

    private boolean validateRole(HttpServletResponse response, Auth annotation, List<String> role) {
        boolean isRole = role.stream().anyMatch(r -> r.equals(annotation.hasRole()));
        if (role.isEmpty() || !isRole) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            logger.error(msg("app.security.validator.roll.message", role, annotation.hasRole()));
            return true;
        }
        return false;
    }

    public Method[] getControllersMethods() {

        List<String> classNames;

        try (ScanResult scanResult = new ClassGraph().acceptPackages("com.acl.work.prices.apiprices.dist.rest").enableClassInfo().scan()) {
            classNames = new ArrayList<>(scanResult.getAllClasses().getNames());
        }

        return classNames
                .stream()
                .map(this::instanceClass)
                .filter(Objects::nonNull)
                .map(Class::getMethods)
                .reduce(this::reduce)
                .get();
    }

    private Class<?> instanceClass(String cls) {
        try {
            if (cls.contains("Health") || cls.contains("Auth")) return null;
            return Class.forName(cls).newInstance().getClass();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            logger.warn("Error loading class {}", cls);
            return null;
        }
    }

    private Method[] reduce(Method[] methods1, Method[] methods2) {
        List<Method> methodsOneList = this.filterControllerMethod.apply(methods1);
        methodsOneList.addAll(this.filterControllerMethod.apply(methods2));
        return methodsOneList.toArray(new Method[0]);
    }

    private final Function<Method[], List<Method>> filterControllerMethod = methods -> Arrays
            .stream(methods)
            .filter(this.filterMethodByAnnotation)
            .collect(Collectors.toList());

    private final Predicate<Method> filterMethodByAnnotation = method -> Arrays
            .stream(method.getDeclaredAnnotations())
            .anyMatch(this.filterAnnotation);

    private final Predicate<Annotation> filterAnnotation = annotation -> annotation instanceof PostMapping || annotation instanceof GetMapping;
}
