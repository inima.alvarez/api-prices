package com.acl.work.prices.apiprices.core.exceptions;

import com.acl.work.prices.apiprices.core.GenericResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import static com.acl.work.prices.apiprices.core.facade.error.ErrorCode.DTO_REQUEST_VALIDATION;


@RestControllerAdvice
public class CustomExceptionHandler {


    @ExceptionHandler(Exception.class)
    public final ResponseEntity handleAllExceptions(Exception ex, WebRequest request) {
        var exceptionResponse = new ResponseException();
        exceptionResponse.setInternalCode("500");
        exceptionResponse.setMessage(ex.getMessage());
        return new ResponseEntity<>(new GenericResponse(exceptionResponse), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Manejador de excepciones para los contratos de clases o servicios del microservicio
     *
     * @param ex {@link MethodArgumentNotValidException} una excepcion indicando argumentos invalidos en el contrato de una clase o servicio
     * @return {@link ResponseEntity} se retorna un {@link ResponseException} con la descripcion del error
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity invalidInput(MethodArgumentNotValidException ex) {
        var result = ex.getBindingResult();
        ResponseException exceptionResponse = new ResponseException();
        exceptionResponse.setInternalCode(DTO_REQUEST_VALIDATION.getCode());
        exceptionResponse.setMessage(DTO_REQUEST_VALIDATION.getMsg());
        exceptionResponse.setValidationErrors(result);
        return new ResponseEntity<>(new GenericResponse(exceptionResponse), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BusinessException.class)
    public final ResponseEntity handleBusinessException(BusinessException ex) {
        var exceptionResponse = new ResponseException();
        exceptionResponse.setInternalCode(ex.getErrorCode());
        exceptionResponse.setMessage(ex.getErrorMsg());

        var r = new GenericResponse(exceptionResponse);

        return ex instanceof AuthorizationException
                ? new ResponseEntity<>(r, HttpStatus.FORBIDDEN)
                : ex instanceof CustomNotFoundException
                ? new ResponseEntity<>(r, HttpStatus.BAD_REQUEST)
                : new ResponseEntity<>(r, HttpStatus.BAD_REQUEST);
    }

}
