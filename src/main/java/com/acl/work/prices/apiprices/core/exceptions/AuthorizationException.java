package com.acl.work.prices.apiprices.core.exceptions;

import com.acl.work.prices.apiprices.core.facade.error.ErrorCode;

import static com.acl.work.prices.apiprices.core.facade.error.ErrorCode.INVALID_TOKEN;

public class AuthorizationException extends BusinessException {

    private static final long serialVersionUID = 5760507686814975116L;

    public AuthorizationException() {
        super(INVALID_TOKEN);
    }

    public AuthorizationException(ErrorCode code) {
        super(code);
    }

    public AuthorizationException(ErrorCode code, String detail) {
        super(code, detail);
    }

    public AuthorizationException(ErrorCode code, Object v) {
        super(code, String.valueOf(v));
    }
}
