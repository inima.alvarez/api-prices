package com.acl.work.prices.apiprices.service.prices.impl;

import com.acl.work.prices.apiprices.service.prices.PriceService;
import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;
import com.acl.work.prices.apiprices.service.prices.contract.to.PriceCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PriceResponseTO;
import com.acl.work.prices.apiprices.service.prices.datasource.PriceRepository;
import com.acl.work.prices.apiprices.service.prices.datasource.domain.PriceTemplate;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.function.Function;

import static com.acl.work.prices.apiprices.service.prices.contract.PriceBinder.PRICE_BINDER;
import static com.acl.work.prices.apiprices.service.prices.contract.QueryBinder.queryBuilder;
import static java.time.format.DateTimeFormatter.ofPattern;

@Service("priceService")
public class PriceServiceImpl implements PriceService {
    private final PriceRepository priceRepository;
    private final JdbcTemplate jdbcTemplate;
    @PersistenceContext
    private EntityManager entityManager;

    public PriceServiceImpl(
            PriceRepository priceRepository,
            JdbcTemplate jdbcTemplate
    ) {
        this.priceRepository = priceRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public PageList<PriceResponseTO> loadAll(PriceCriteriaTO criteria) {
        var sql = queryBuilder.apply(criteria);
        var result = this.jdbcTemplate.query(sql, BeanPropertyRowMapper.newInstance(PriceTemplate.class));
        return PRICE_BINDER.bindFromTemplate(result);
    }

    public static final Function<LocalDateTime, String> formatDateTimeTOISODate = date -> date.format(ofPattern("yyyy-MM-dd HH:mm:ss"));
}
