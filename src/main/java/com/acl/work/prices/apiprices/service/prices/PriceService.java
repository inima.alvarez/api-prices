package com.acl.work.prices.apiprices.service.prices;

import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;
import com.acl.work.prices.apiprices.service.prices.contract.to.PriceCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PriceResponseTO;

/**
 * Default interface for {@link PriceService}
 * Perform operations over price repository
 *
 * @author Inima
 * @since 0.1.0
 */
public interface PriceService {

    PageList<PriceResponseTO> loadAll(PriceCriteriaTO criteria);
}
