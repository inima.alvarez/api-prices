package com.acl.work.prices.apiprices.service.prices.impl;

import com.acl.work.prices.apiprices.core.exceptions.CustomNotFoundException;
import com.acl.work.prices.apiprices.core.facade.error.ErrorCode;
import com.acl.work.prices.apiprices.service.prices.BrandService;
import com.acl.work.prices.apiprices.service.prices.contract.to.BrandCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.BrandTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;
import com.acl.work.prices.apiprices.service.prices.datasource.BrandRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.acl.work.prices.apiprices.service.prices.contract.BrandBinder.BRAND_BINDER;

@Service("brandService")
public class BrandServiceImpl implements BrandService {

    private final BrandRepository brandRepository;

    public BrandServiceImpl(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }

    @Override
    public BrandTO create(BrandTO to) {
        var entity = BRAND_BINDER.bind(to);
        var saved = this.brandRepository.save(entity);
        return BRAND_BINDER.bind(saved);
    }

    @Override
    public BrandTO get(Integer id) {
        return this.brandRepository.findById((long) id)
                .map(BRAND_BINDER::bind)
                .orElse(new BrandTO());
    }

    @Override
    public BrandTO update(BrandTO to) {
        return this.brandRepository.findById((long) to.getId())
                .map(entity -> BRAND_BINDER.bind(entity, to))
                .flatMap(brand -> Optional.of(this.brandRepository.save(brand)))
                .map(BRAND_BINDER::bind)
                .orElse(new BrandTO());
    }

    @Override
    public void delete(Integer id) {
        var brand = this.brandRepository.findById((long) id)
                .orElseThrow(() -> new CustomNotFoundException(ErrorCode.NOT_FOUND));
        this.brandRepository.delete(brand);
    }

    @Override
    public PageList<BrandTO> loadAll(BrandCriteriaTO criteria) {
        var source = criteria.getCriteria();
        var brandList = source
                .equals("*")
                ? this.brandRepository.findAll()
                : this.brandRepository.findAllByNameLike(source);
        return BRAND_BINDER.bind(brandList);
    }
}
