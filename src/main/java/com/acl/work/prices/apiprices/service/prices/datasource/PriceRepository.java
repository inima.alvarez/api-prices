package com.acl.work.prices.apiprices.service.prices.datasource;

import com.acl.work.prices.apiprices.service.prices.datasource.domain.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("priceRepository")
public interface PriceRepository extends JpaRepository<Price, Long> {

    @Query(nativeQuery = true, value = "select * from prices p inner join taxes t on p.price_list = t.id inner join product pr on p.product_id = pr.id and pr.id = 35455 inner join brand br on p.brand_id = br.id and br.id = 1 and concat('[',concat(start_date , concat(',', concat(end_date , ']'))))::tsrange @> '2020-06-14 10:00:00'::timestamp")
    List<Price> findAllByProductAndBrandAndEndDateAndStartDate(Long product, Long brand, String dateRange);
}
