package com.acl.work.prices.apiprices.service.prices.contract;

import com.acl.work.prices.apiprices.service.prices.contract.to.ProductTO;
import com.acl.work.prices.apiprices.service.prices.datasource.domain.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ProductBinder {
    ProductBinder PRODUCT_BINDER = Mappers.getMapper(ProductBinder.class);

    Product bind(ProductTO brand);

    @Mapping(target = "withId", source = "id")
    ProductTO bind(Product brand);

    List<ProductTO> bind(List<Product> source);

    @Mapping(target = "id", ignore = true)
    Product bind(@MappingTarget Product target, ProductTO source);

    List<Product> bindBulk(List<ProductTO> source);
}
