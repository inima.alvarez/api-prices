package com.acl.work.prices.apiprices.service.security.contract.to;

import com.acl.work.prices.apiprices.core.TransferObject;

public class RoleTO extends TransferObject {

    private Long id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
