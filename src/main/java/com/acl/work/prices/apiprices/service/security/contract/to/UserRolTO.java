package com.acl.work.prices.apiprices.service.security.contract.to;

import com.acl.work.prices.apiprices.core.TransferObject;

import java.util.List;

public class UserRolTO extends TransferObject {
    private UserTO user;
    private List<RoleTO> rol;
    private String token;

    public UserTO getUser() {
        return user;
    }

    public void setUser(UserTO user) {
        this.user = user;
    }

    public List<RoleTO> getRol() {
        return rol;
    }

    public void setRol(List<RoleTO> rol) {
        this.rol = rol;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserRolTO withToken(String token) {
        this.setToken(token);
        return this;
    }
}
