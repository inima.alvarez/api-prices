package com.acl.work.prices.apiprices.service.prices.contract.to;

import com.acl.work.prices.apiprices.core.TransferObject;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TaxTO extends TransferObject {
    @JsonProperty("apply_tax")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
