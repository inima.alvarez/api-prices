package com.acl.work.prices.apiprices.service.security.datasource;

import com.acl.work.prices.apiprices.service.security.datasource.domain.User;
import com.acl.work.prices.apiprices.service.security.datasource.domain.UserRol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select p from User p where p.username = :username")
    User findByName(@Param("username") String username);
}
