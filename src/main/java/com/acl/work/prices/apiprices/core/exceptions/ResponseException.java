package com.acl.work.prices.apiprices.core.exceptions;

import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Estructura CENCOSUD para los errores
 *
 * @author max.martinez
 */
public class ResponseException {

    private String internalCode;
    private String message;
    private final List<SubErrorResponse> errors = new ArrayList<>();

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SubErrorResponse> getErrors() {
        return errors;
    }

    public void setValidationErrors(Errors errs) {
        errors.addAll(errs.getGlobalErrors()
                .stream()
                .map(this::buildErrorResponse)
                .collect(toList()));

        errors.addAll(errs.getFieldErrors()
                .stream()
                .map(this::buildErrorResponse)
                .collect(toList()));
    }

    private SubErrorResponse buildErrorResponse(FieldError f) {
        return new SubErrorResponse(f.getField(), f.getRejectedValue(), f.getDefaultMessage(), f.getObjectName());
    }

    private SubErrorResponse buildErrorResponse(ObjectError err) {
        return new SubErrorResponse(err.getCode(), "Global", err.getDefaultMessage(), err.getObjectName());
    }

    public static class SubErrorResponse {
        String internalErrorCode;
        Object developerMessage;
        String userMessage;
        String moreInfo;

        public SubErrorResponse(String internalErrorCode, Object developerMessage, String userMessage,
                                String moreInfo) {
            super();
            this.internalErrorCode = internalErrorCode;
            this.developerMessage = developerMessage;
            this.userMessage = userMessage;
            this.moreInfo = moreInfo;
        }

        public String getInternalErrorCode() {
            return internalErrorCode;
        }

        public void setInternalErrorCode(String internalErrorCode) {
            this.internalErrorCode = internalErrorCode;
        }

        public Object getDeveloperMessage() {
            return developerMessage;
        }

        public void setDeveloperMessage(Object developerMessage) {
            this.developerMessage = developerMessage;
        }

        public String getUserMessage() {
            return userMessage;
        }

        public void setUserMessage(String userMessage) {
            this.userMessage = userMessage;
        }

        public String getMoreInfo() {
            return moreInfo;
        }

        public void setMoreInfo(String moreInfo) {
            this.moreInfo = moreInfo;
        }
    }
}
