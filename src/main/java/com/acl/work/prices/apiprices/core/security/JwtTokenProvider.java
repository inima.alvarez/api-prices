package com.acl.work.prices.apiprices.core.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;
import java.security.InvalidKeyException;
import java.security.cert.CertificateException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.acl.work.prices.apiprices.util.msg.MessageContextHolder.msg;
import static java.lang.String.format;


@Component
public class JwtTokenProvider {
    private final Logger log = LoggerFactory.getLogger(JwtTokenProvider.class);
    private static final String HEADER_TOKEN = "Authorization";
    private static final String TOKEN_NAME = "Bearer";
    public static final long JWT_TOKEN_VALIDITY = 2;

    private final static ZoneId zone = ZoneId.systemDefault();
    private final Date tokenValidity = Date
            .from(LocalDateTime
                    .now()
                    .plus(JWT_TOKEN_VALIDITY, ChronoUnit.HOURS)
                    .atZone(zone)
                    .toInstant()
            );

    private final Date tokenIssue = Date.from(LocalDateTime.now().atZone(zone).toInstant());
    private final String secretPhrase;

    public JwtTokenProvider(@Value("${jwt.secret}") String secretPhrase) {
        this.secretPhrase = secretPhrase;
    }

    public Authentication getAuthentication(String token) throws InvalidKeyException, CertificateException {

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("USER"));

        var userDetails = User.withUsername(getUsername(token))
                .password(String.valueOf(Math.random() * 5))
                .authorities(authorities)
                .accountExpired(false)
                .accountLocked(false)
                .credentialsExpired(false)
                .disabled(false)
                .build();

        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public String generateJwtToken(String username, List<String> roles) {
        try {
            var claims = this.bindClaims(username, roles);
            return Jwts.builder().setClaims(claims).setSubject(username)
                    .setIssuedAt(tokenIssue)
                    .setExpiration(tokenValidity)
                    .signWith(SignatureAlgorithm.HS512, secretPhrase).compact();
        } catch (Exception e) {
            log.error(msg("app.message.token.generation.error", e.getMessage()));
            return null;
        }
    }

    private Map<String, Object> bindClaims(String username, List<String> roles) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", username);
        claims.put("sub", username);
        claims.put("roles", roles);
        return claims;
    }

    /**
     * Obtiene el username del token
     *
     * @param token
     * @return
     * @throws Exception
     */
    public String getUsername(String token) throws CertificateException, InvalidKeyException {
        return this.getTokenClaims(token).getSubject();
    }

    /**
     * Obtiene el token desde el header del request
     *
     * @param req
     * @return
     */
    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader(HEADER_TOKEN);
        return Objects.nonNull(bearerToken) && bearerToken.startsWith(TOKEN_NAME.concat(" "))
                ? bearerToken.substring(7, bearerToken.length())
                : null;
    }


    public boolean validateToken(String token) {
        try {
            this.getTokenClaims(token);
            return true;
        } catch (SignatureException | MalformedJwtException | ExpiredJwtException | UnsupportedJwtException | CertificateException ex) {
            String message = format("Invalid token definition %s %s", ex.getClass(), ex.getMessage());
            log.error(message);
            throw new ValidationException(message);
        }
    }

    protected Claims getTokenClaims(String token) throws CertificateException {
        return Jwts
                .parser()
                .setSigningKey(this.secretPhrase)
                .parseClaimsJws(token)
                .getBody();
    }

    public List<String> getRoleFromToken(String token) {
        try {
            final Claims claims = this.getTokenClaims(token);
            var role = claims.get("roles");
            return role instanceof List
                    ? (List<String>) role
                    : new ArrayList<>();
        } catch (Exception ignored) {
        }
        return new ArrayList<>();
    }

}
