package com.acl.work.prices.apiprices.service.security.contract.to;

import com.acl.work.prices.apiprices.core.TransferObject;

public class UserRequestTO extends TransferObject {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
