package com.acl.work.prices.apiprices.core.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.acl.work.prices.apiprices.service.*.contract.validation"})
public class ValidationConfiguration {
}
