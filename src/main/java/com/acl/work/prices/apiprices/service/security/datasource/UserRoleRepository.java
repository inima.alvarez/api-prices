package com.acl.work.prices.apiprices.service.security.datasource;

import com.acl.work.prices.apiprices.service.security.datasource.domain.UserRol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userRoleRepository")
public interface UserRoleRepository extends JpaRepository<UserRol, Long> {

    @Query("select p from UserRol p join p.user u where u.username = :username")
    List<UserRol> findByName(@Param("username") String username);
}
