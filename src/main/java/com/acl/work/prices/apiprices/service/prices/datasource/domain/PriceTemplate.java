package com.acl.work.prices.apiprices.service.prices.datasource.domain;

import java.time.LocalDateTime;

public class PriceTemplate {
    private Long id;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Long priority;
    private Double price;
    private String curr;
    private Long taxId;
    private String taxName;
    private Long productId;
    private String productName;
    private String productCode;
    private String brandId;
    private String brandName;

    public PriceTemplate() {
    }

    public PriceTemplate(Long id, LocalDateTime startDate, LocalDateTime endDate, Long priority, Double price, String curr, Long taxId, String taxName, Long productId, String productName, String productCode, String brandId, String brandName) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.priority = priority;
        this.price = price;
        this.curr = curr;
        this.taxId = taxId;
        this.taxName = taxName;
        this.productId = productId;
        this.productName = productName;
        this.productCode = productCode;
        this.brandId = brandId;
        this.brandName = brandName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCurr() {
        return curr;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }

    public Long getTaxId() {
        return taxId;
    }

    public void setTaxId(Long taxId) {
        this.taxId = taxId;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
