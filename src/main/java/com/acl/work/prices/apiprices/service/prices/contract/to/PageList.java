package com.acl.work.prices.apiprices.service.prices.contract.to;

import com.acl.work.prices.apiprices.core.TransferObject;

import java.util.List;

public class PageList<T> extends TransferObject {
    private Integer size;
    private List<T> elements;

    public Integer getSize() {
        return this.elements.size();
    }

    private void setSize(Integer size) {
        this.size = size;
    }

    public List<T> getElements() {
        return elements;
    }

    private void setElements(List<T> elements) {
        this.elements = elements;
    }

    public PageList<T> withElements(List<T> elements) {
        this.setElements(elements);
        return this;
    }
}
