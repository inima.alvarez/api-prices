package com.acl.work.prices.apiprices.service.prices.contract.validation;

import com.acl.work.prices.apiprices.core.exceptions.BusinessException;
import com.acl.work.prices.apiprices.service.prices.contract.to.ProductTO;
import com.acl.work.prices.apiprices.service.prices.datasource.ProductRepository;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static com.acl.work.prices.apiprices.core.facade.error.ErrorCode.BAD_REQUEST;
import static com.acl.work.prices.apiprices.util.msg.MessageContextHolder.msg;

@Aspect
@Component
public class CreateProductValidation {
    private final static String DEFAULT_ARGS = "joinPoint, to";
    private final Logger logger = LoggerFactory.getLogger(CreateProductValidation.class);

    private final ProductRepository productRepository;

    public CreateProductValidation(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @Before(value = "execution(* com.acl.work.prices.apiprices.service.prices.ProductService+.create(..)) && args(to)", argNames = DEFAULT_ARGS)
    public void beforeAdvice(JoinPoint joinPoint, ProductTO to) throws BusinessException {

        var product = productRepository.findByName(to.getName());

        if (Objects.nonNull(product) && product.getCodigo().equals(to.getCodigo())) {
            logger.error(msg("app.message.product.create.found", to.getName(), to.getCodigo()));
            throw new BusinessException(BAD_REQUEST, msg("app.message.product.create.found", to.getName(), to.getCodigo()));
        }
    }
}
