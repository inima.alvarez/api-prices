package com.acl.work.prices.apiprices.service.prices.contract;

import com.acl.work.prices.apiprices.service.prices.contract.to.PriceCriteriaTO;

import java.time.LocalDateTime;
import java.util.function.Function;

import static java.lang.String.format;
import static java.time.format.DateTimeFormatter.ofPattern;

public class QueryBinder {
    private static final String fields = "p.id, p.start_date, p.end_date, p.priority, p.price, p.curr, t.id as tax_id, t.\"name\" as tax_name, pr.id as product_id, pr.\"name\" as product_name, pr.codigo as product_code, br.id as brand_id, br.\"name\" as brand_name";
    private static final String query = "select %s from prices p inner join taxes t on p.price_list = t.id inner join product pr on p.product_id = pr.id and pr.id = %s inner join brand br on p.brand_id = br.id and br.id = %s and concat('[',concat(start_date , concat(',', concat(end_date , ']'))))::tsrange @> '%s'::timestamp";

    private static final Function<LocalDateTime, String> formatDateTimeTOISODate = date -> date.format(ofPattern("yyyy-MM-dd HH:mm:ss"));

    public static final Function<PriceCriteriaTO, String> queryBuilder = criteria ->  format(query, fields, criteria.getProduct(), criteria.getBrand(), formatDateTimeTOISODate.apply(criteria.getApplicationDate()));
}
