package com.acl.work.prices.apiprices.service.prices;

import com.acl.work.prices.apiprices.service.prices.contract.to.BrandCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.BrandTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;

import java.util.List;

/**
 * Default interface for {@link BrandService}
 * Perform operations over brand repository
 *
 * @author Inima
 * @since 0.1.0
 */
public interface BrandService {
    BrandTO create(BrandTO to);

    BrandTO get(Integer id);

    BrandTO update(BrandTO to);

    void delete(Integer id);

    PageList<BrandTO> loadAll(BrandCriteriaTO criteria);
}
