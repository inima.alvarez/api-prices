package com.acl.work.prices.apiprices.service.security.contract;

import com.acl.work.prices.apiprices.service.security.contract.to.RoleTO;
import com.acl.work.prices.apiprices.service.security.contract.to.UserRequestTO;
import com.acl.work.prices.apiprices.service.security.contract.to.UserRolTO;
import com.acl.work.prices.apiprices.service.security.contract.to.UserTO;
import com.acl.work.prices.apiprices.service.security.datasource.domain.Rol;
import com.acl.work.prices.apiprices.service.security.datasource.domain.User;
import com.acl.work.prices.apiprices.service.security.datasource.domain.UserRol;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

@Mapper
public interface UserBinder {
    UserBinder USER_BINDER = Mappers.getMapper(UserBinder.class);

    User bind(UserRequestTO source);

    UserTO bind(User source);

    RoleTO bind(Rol source);

    @Mapping(target = "user", expression = "java(this.bind(source.getUser()))")
    @Mapping(target = "rol", ignore = true)
    UserRolTO bind(UserRol source);

    default UserRolTO bind(List<UserRol> source) {
        var target = source
                .stream()
                .map(this::bind)
                .findFirst()
                .orElse(null);

        if (Objects.isNull(target)) return new UserRolTO();

        var roles = source.stream()
                .map(UserRol::getRol)
                .map(this::bind)
                .collect(toList());

        target.setRol(roles);
        return target;
    }
}
