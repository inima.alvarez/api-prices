package com.acl.work.prices.apiprices.dist.rest;

import com.acl.work.prices.apiprices.core.facade.controllers.Controller;
import com.acl.work.prices.apiprices.core.security.stereotypes.Auth;
import com.acl.work.prices.apiprices.facade.ProductFacade;
import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;
import com.acl.work.prices.apiprices.service.prices.contract.to.ProductCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.ProductTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/private/product/")
@Tag(name = "product", description = "API to manage product")
public class ProductController extends Controller<ProductFacade> {

    @Operation(summary = "Create a single product", description = "Create a single product", tags = {"product"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = ProductResponse.class))),
            @ApiResponse(responseCode = "404", description = "Exist a product with the same name")
    })
    @PostMapping
    @Auth(hasRole = "USER")
    public ResponseEntity create(@RequestBody @Validated ProductTO to) {
        return this.get(facade -> facade.create(to));
    }

    @Operation(summary = "Get product by id", description = "Load a single product by id", tags = {"product"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = ProductResponse.class))),
            @ApiResponse(responseCode = "404", description = "Product not found")
    })
    @GetMapping("{id}")
    @Auth(hasRole = "USER")
    public ResponseEntity get(@PathVariable String id) {
        return this.get(facade -> facade.load(Integer.parseInt(id)));
    }

    @Operation(summary = "Update a product", description = "Update a single product", tags = {"product"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = ProductResponse.class))),
            @ApiResponse(responseCode = "404", description = "Product not found")
    })
    @PutMapping("{id}")
    @Auth(hasRole = "USER")
    public ResponseEntity update(@PathVariable String id, @RequestBody @Validated ProductTO to) {
        return this.get(facade -> facade.update(to.withId(Integer.parseInt(id))));
    }

    @Operation(summary = "Delete a product by a id", description = "Delete a single product", tags = {"product"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema())),
            @ApiResponse(responseCode = "404", description = "Product not found")
    })
    @DeleteMapping("{id}")
    @Auth(hasRole = "USER")
    public ResponseEntity delete(@PathVariable String id) {
        return this.get(facade -> facade.delete(Integer.parseInt(id)));
    }

    @Operation(summary = "Search a product by a given criteria", description = "Name search by %name% format", tags = {"product"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = ProductListResponse.class)))
    })
    @PostMapping("search")
    @Auth(hasRole = "USER")
    public ResponseEntity search(@RequestBody @Validated ProductCriteriaTO criteria) {
        return this.get(facade -> facade.search(criteria));
    }

    private static class ProductListResponse extends PageList<ProductTO> {
    }


    private static class ProductResponse extends ProductTO {
    }
}
