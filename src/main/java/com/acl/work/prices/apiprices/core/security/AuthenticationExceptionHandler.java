package com.acl.work.prices.apiprices.core.security;

import com.acl.work.prices.apiprices.core.GenericResponse;
import com.acl.work.prices.apiprices.core.exceptions.ResponseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;

@Component
public class AuthenticationExceptionHandler implements AuthenticationEntryPoint, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -3463545595000532154L;
    @SuppressWarnings("squid:S1948")
    private final Logger log = LoggerFactory.getLogger(AuthenticationExceptionHandler.class);

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) {

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding(StandardCharsets.UTF_8.toString());
        response.setStatus(HttpStatus.UNAUTHORIZED.value());

        ResponseException responseException = new ResponseException();
        responseException.setInternalCode(HttpStatus.UNAUTHORIZED.name());
        responseException.setMessage(authException.getMessage());

        try {
            ObjectMapper mapper = new ObjectMapper();
            response.getWriter().write(mapper.writeValueAsString(new GenericResponse(responseException)));
        } catch (Exception e1) {
            log.error("Error: ", e1);
        }
    }
}
