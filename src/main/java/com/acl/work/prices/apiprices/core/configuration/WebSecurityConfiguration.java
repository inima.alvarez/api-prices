package com.acl.work.prices.apiprices.core.configuration;

import com.acl.work.prices.apiprices.core.security.AuthenticationExceptionHandler;
import com.acl.work.prices.apiprices.core.security.JwtTokenFilterConfig;
import com.acl.work.prices.apiprices.core.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import static org.springframework.http.HttpMethod.POST;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final String servletPath;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationExceptionHandler authenticationExceptionHandler;

    public WebSecurityConfiguration(@Value("${spring.mvc.servlet.path}") String servletPath,
                                    JwtTokenProvider jwtTokenProvider,
                                    AuthenticationExceptionHandler authenticationExceptionHandler) {
        this.servletPath = servletPath;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationExceptionHandler = authenticationExceptionHandler;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()

                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()

                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()

                .antMatchers(this.servletPath.concat("/publico/logging/**")).permitAll()

                .anyRequest().authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(authenticationExceptionHandler)
                .and()

                .apply(new JwtTokenFilterConfig(jwtTokenProvider));
    }

    @Override
    public void configure(WebSecurity web)  {
        web.ignoring()
                .antMatchers(this.servletPath.concat("/swagger-ui/**"))
                .antMatchers(this.servletPath.concat("/v3/api-docs/**"))
                .antMatchers(this.servletPath.concat("/swagger-resources/**"))
                .antMatchers(this.servletPath.concat("/swagger-ui.html"))
                .antMatchers(this.servletPath.concat("/configuration/**"))
                .antMatchers(this.servletPath.concat("/webjars/**"))
                .antMatchers(this.servletPath.concat("/public"));
    }
}
