package com.acl.work.prices.apiprices.service.prices.contract.to;

import com.acl.work.prices.apiprices.core.TransferObject;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductTO extends TransferObject {
    private Integer id;
    private String name;
    @JsonProperty("code")
    private String codigo;

    public Integer getId() {
        return id;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public ProductTO withId(Integer id) {
        this.setId(id);
        return this;
    }
}
