package com.acl.work.prices.apiprices.service.prices.datasource;

import com.acl.work.prices.apiprices.service.prices.datasource.domain.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("brandRepository")
public interface BrandRepository extends JpaRepository<Brand, Long> {

    @Query("select p from Brand p where p.name = :name")
    Brand findByName(@Param("name") String name);

    List<Brand> findAllByNameLike(String name);
}
