package com.acl.work.prices.apiprices.core.configuration;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class DatasourceConfiguration {

    private final String password;

    public DatasourceConfiguration(@Value("${datasource.password}")String password) {
        this.password = password;
    }

    @Bean
    @ConfigurationProperties("spring.datasource")
    public DataSourceProperties dataSourceProperties() {
        DataSourceProperties secondProperties = new DataSourceProperties();
        secondProperties.setPassword(this.password);
        return secondProperties;
    }

    // Define el data source de la segunda bd
    @Bean("priceJdbc")
    @Primary
    @ConfigurationProperties("spring.second.datasource.configuration")
    public HikariDataSource hikariDataSource() {
        return dataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }

    @Bean(name = "pricesTemplate")
    public JdbcTemplate jdbcTemplate(@Qualifier("priceJdbc") DataSource hikariDataSource) {
        return new JdbcTemplate(hikariDataSource);
    }
}
