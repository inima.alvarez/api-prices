package com.acl.work.prices.apiprices.core.security.role;

public class Role {
    public static final String USER = "USER";
    public static final String ADMIN = "ADMIN";

    private Role() {
    }
}
