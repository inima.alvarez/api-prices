package com.acl.work.prices.apiprices.core.facade.error;

import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;
import static java.util.Arrays.stream;

public enum ErrorCode {
    // Internal errors
    INTERNAL_ERROR(300,"INTERNAL_ERROR", "Internal Server Error"),


    //security errors
    FORBIDDEN_ACCESS(200,"FORBIDDEN", "Forbidden"),
    INVALID_TOKEN(201,"INVALID_TOKEN", "Token no valido: %s"),
    INVALID_TOKEN_ERROR(202,"INVALID_TOKEN_ERROR", "Error jwt token"),
    INVALID_TOKEN_SIGNATURE(203,"INVALID_TOKEN_SIGNATURE", "Firma del token no valida"),
    INVALID_TOKEN_EXPIRED(204,"INVALID_TOKEN_EXPIRED", "Token expirado"),
    INVALID_TOKEN_MALFORMED(205,"INVALID_TOKEN_MALFORMED", "Estructura del Token no valido"),
    GOOGLE_CAPTCHA_NOT_VALID(206,"GOOGLE_CAPTCHA_NOT_VALID", "%s"),

    DTO_REQUEST_VALIDATION(102,"DTO_REQUEST_VALIDATION", "Atributos no validos"),
    BAD_REQUEST(103,"BAD_REQUEST", "The request is invalid or malformed %s"),
    INVALID_REFERENCE(104,"INVALID_REFERENCE", "Element no found"),
    NOT_FOUND(105,"NOT_FOUND", "Resource not found %s"),


    INTERNAL_SERVER(135,"INTERNAL", "Error interno del servidor"),

    ;


    private final int index;
    private final String code;
    private final String msg;

    private static final Map<String, ErrorCode> nameIndex = new HashMap<>();

    static {
        for (ErrorCode suit : ErrorCode.values()) {
            nameIndex.put(suit.getCode(), suit);
        }
    }

    ErrorCode(int index, String code, String msg) {
        this.index = index;
        this.code = code;
        this.msg = msg;
    }

    public static ErrorCode lookupByName(String name) {
        return nameIndex.get(name);
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public int getIndex(){
        return index;
    }

    public boolean is1xxValidation() {
        return Series.VALIDATION.equals(series());
    }


    public boolean is2xxSecurity() {
        return Series.SECURITY.equals(series());
    }


    public boolean is3xxBackEnd() {
        return Series.BACKEND.equals(series());
    }


    public Series series() {
        return Series.valueOf(this);
    }


    public enum Series {
        VALIDATION(1),
        SECURITY(2),
        BACKEND(3);

        private final int value;

        Series(int value) {
            this.value = value;
        }

        public static Series valueOf(int status) {
            int seriesCode = status / 100;
            return stream(values())
                    .filter(series -> series.value == seriesCode)
                    .findAny()
                    .orElseThrow(() -> new IllegalArgumentException(format("No matching constant for '%s'", status)));
        }

        public static Series valueOf(ErrorCode code) {
            return valueOf(code.index);
        }


        public int value() {
            return this.value;
        }
    }
}
