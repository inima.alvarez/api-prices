package com.acl.work.prices.apiprices.service.prices.contract.validation;

import com.acl.work.prices.apiprices.core.exceptions.CustomNotFoundException;
import com.acl.work.prices.apiprices.core.facade.error.ErrorCode;
import com.acl.work.prices.apiprices.service.prices.contract.to.BrandTO;
import com.acl.work.prices.apiprices.service.prices.datasource.BrandRepository;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static com.acl.work.prices.apiprices.util.msg.MessageContextHolder.msg;

@Aspect
@Component
public class UpdateBrandValidation {
    private final static String DEFAULT_ARGS = "joinPoint, to";
    private final Logger logger = LoggerFactory.getLogger(UpdateBrandValidation.class);
    private final BrandRepository brandRepository;

    public UpdateBrandValidation(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }

    @Before(value = "execution(* com.acl.work.prices.apiprices.service.prices.BrandService+.update(..)) && args(to)", argNames = DEFAULT_ARGS)
    public void beforeAdvice(JoinPoint joinPoint, BrandTO to) throws CustomNotFoundException {
        this.brandRepository.findById((long) to.getId())
                .orElseThrow(() -> {
                    logger.error(msg("app.message.brand.not.found", to.getId()));
                    throw new CustomNotFoundException(ErrorCode.NOT_FOUND, msg("app.message.brand.not.found", to.getId()));
                });
    }
}
