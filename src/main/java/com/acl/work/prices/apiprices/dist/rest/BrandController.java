package com.acl.work.prices.apiprices.dist.rest;

import com.acl.work.prices.apiprices.core.facade.controllers.Controller;
import com.acl.work.prices.apiprices.core.security.stereotypes.Auth;
import com.acl.work.prices.apiprices.facade.BrandFacade;
import com.acl.work.prices.apiprices.service.prices.contract.to.BrandCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.BrandTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/private/brand/")
@Tag(name = "brand", description = "API to manage brands")
public class BrandController extends Controller<BrandFacade> {

    @Operation(summary = "Create a single brand", description = "Create a single brand", tags = {"brand"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = BrandResponse.class))),
            @ApiResponse(responseCode = "404", description = "Exist a brand with the same name")
    })
    @PostMapping
    @Auth(hasRole = "USER")
    public ResponseEntity create(@RequestBody @Validated BrandTO to) {
        return this.get(facade -> facade.create(to));
    }

    @Operation(summary = "Get brand by id", description = "Load a single brand by id", tags = {"brand"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = BrandResponse.class))),
            @ApiResponse(responseCode = "404", description = "Brand not found")
    })
    @GetMapping("{id}")
    @Auth(hasRole = "USER")
    public ResponseEntity get(@PathVariable String id) {
        return this.get(facade -> facade.load(Integer.parseInt(id)));
    }

    @Operation(summary = "Update a brand", description = "Update a single brand", tags = {"brand"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = BrandResponse.class))),
            @ApiResponse(responseCode = "404", description = "Brand not found")
    })
    @PutMapping("{id}")
    @Auth(hasRole = "USER")
    public ResponseEntity update(@PathVariable String id, @RequestBody @Validated BrandTO to) {
        return this.get(facade -> facade.update(to.withId(Integer.parseInt(id))));
    }

    @Operation(summary = "Delete a brand by a id", description = "Delete a single contact", tags = {"brand"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema())),
            @ApiResponse(responseCode = "404", description = "Brand not found")
    })
    @DeleteMapping("{id}")
    @Auth(hasRole = "USER")
    public ResponseEntity delete(@PathVariable String id) {
        return this.get(facade -> facade.delete(Integer.parseInt(id)));
    }


    @Operation(summary = "Search a brand by a given criteria", description = "Name search by %name% format", tags = {"brand"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = BrandPageResponse.class))),
            @ApiResponse(responseCode = "404", description = "Brand not found")
    })
    @PostMapping("search")
    @Auth(hasRole = "USER")
    public ResponseEntity search(@RequestBody @Validated BrandCriteriaTO criteria) {
        return this.get(facade -> facade.search(criteria));
    }

    private static class BrandPageResponse extends PageList<BrandTO> {
    }

    private static class BrandResponse extends BrandTO {
    }
}
