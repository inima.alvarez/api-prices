package com.acl.work.prices.apiprices.util.json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.util.stream.Stream;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import static java.lang.String.format;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;
import static org.slf4j.LoggerFactory.getLogger;

public class JsonSerdes {
    private static final Logger logger = getLogger(JsonSerdes.class);
    private static final ObjectMapper MAPPER;

    static {
        MAPPER = Jackson2ObjectMapperBuilder.json()
                .serializationInclusion(NON_EMPTY)
                .featuresToDisable(WRITE_DATES_AS_TIMESTAMPS)
                .modules(new JavaTimeModule())
                .build();
    }

    private JsonSerdes() {
        throw new AssertionError("No 'JsonSerdes' instances for you!");
    }


    static public String jsonfy(String[] array, boolean includeQuotes) {
        Stream<String> stream = includeQuotes ? stream(array).map(element -> format("\"%s\"", element)) : stream(array);
        return stream.collect(joining(",", "[", "]"));
    }

    static public String jsonfy(Object obj) {
        try {
            return MAPPER.writeValueAsString(obj);
        } catch (Exception ex) {
            logger.trace("Error detected when trying to covert an object to JSON", ex);
            return null;
        }
    }

    static public ObjectMapper mapper() {
        return MAPPER;
    }
}
