package com.acl.work.prices.apiprices.configuration;

import com.acl.work.prices.apiprices.core.configuration.FacadeContext;
import com.acl.work.prices.apiprices.core.configuration.FlywayConfiguration;
import com.acl.work.prices.apiprices.core.configuration.SwaggerConfiguration;
import com.acl.work.prices.apiprices.core.configuration.ValidationConfiguration;
import com.acl.work.prices.apiprices.core.configuration.WebSecurityConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import static com.acl.work.prices.apiprices.util.msg.MessageContextHolder.source;


@Configuration
@ComponentScan({
        "com.acl.work.prices.apiprices.dist.rest",
        "com.acl.work.prices.apiprices.core.exceptions",
        "com.acl.work.prices.apiprices.core.security",
        "com.acl.work.prices.apiprices.facade.impl",
        "com.acl.work.prices.apiprices.service.*.impl",
        "com.acl.work.prices.apiprices.service.*.datasource"
})
@Import({
        FacadeContext.class,
        SwaggerConfiguration.class,
        WebSecurityConfiguration.class,
        ValidationConfiguration.class,
        FlywayConfiguration.class
})
public class DefaultConfiguration {

    @Bean
    public MessageSource messageSource() {
        return source();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }
}
