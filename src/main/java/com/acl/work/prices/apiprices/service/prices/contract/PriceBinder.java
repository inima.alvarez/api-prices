package com.acl.work.prices.apiprices.service.prices.contract;

import com.acl.work.prices.apiprices.service.prices.contract.to.BrandTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;
import com.acl.work.prices.apiprices.service.prices.contract.to.PriceResponseTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.ProductTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.TaxTO;
import com.acl.work.prices.apiprices.service.prices.datasource.domain.PriceTemplate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(imports = {
        ZoneId.class
})
public interface PriceBinder {
    PriceBinder PRICE_BINDER = Mappers.getMapper(PriceBinder.class);


    @Mapping(target = "withId", source = "brandId")
    @Mapping(target = "name", source = "brandName")
    BrandTO bindBrand(PriceTemplate source);

    @Mapping(target = "withId", source = "productId")
    @Mapping(target = "name", source = "productName")
    @Mapping(target = "codigo", source = "productCode")
    ProductTO bindProduct(PriceTemplate source);

    @Mapping(target = "name", source = "taxName")
    TaxTO bindTax(PriceTemplate source);

    @Mapping(target = "applicationStartDate", source = "startDate")
    @Mapping(target = "applicationEndDate", source = "endDate")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "product", expression = "java(this.bindProduct(source))")
    @Mapping(target = "brand", expression = "java(this.bindBrand(source))")
    @Mapping(target = "tax", expression = "java(this.bindTax(source))")
    PriceResponseTO bind(PriceTemplate source);


    default PageList<PriceResponseTO> bindFromTemplate(List<PriceTemplate> source) {
        var target = source
                .stream()
                .map(this::bind)
                .collect(Collectors.toList());
        return new PageList<PriceResponseTO>()
                .withElements(target);
    }
}
