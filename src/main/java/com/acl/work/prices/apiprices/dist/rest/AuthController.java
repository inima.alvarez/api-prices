package com.acl.work.prices.apiprices.dist.rest;

import com.acl.work.prices.apiprices.core.facade.controllers.Controller;
import com.acl.work.prices.apiprices.facade.AuthenticationFacade;
import com.acl.work.prices.apiprices.service.security.contract.to.UserRequestTO;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/publico/logging")
public class AuthController extends Controller<AuthenticationFacade> {

    @Operation(description = "Getting a user logging token")
    @PostMapping("/")
    public ResponseEntity authenticate(@RequestBody @Validated UserRequestTO to) {
        return this.get(facade -> facade.logging(to));
    }
}
