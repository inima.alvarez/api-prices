package com.acl.work.prices.apiprices.facade.impl;

import com.acl.work.prices.apiprices.core.facade.Result;
import com.acl.work.prices.apiprices.core.facade.stereotype.Facade;
import com.acl.work.prices.apiprices.facade.ProductFacade;
import com.acl.work.prices.apiprices.service.prices.ProductService;
import com.acl.work.prices.apiprices.service.prices.contract.to.ProductCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.ProductTO;

import java.util.List;

import static com.acl.work.prices.apiprices.core.facade.Result.successful;

@Facade("productFacade")
public class ProductFacadeImpl implements ProductFacade {

    private final ProductService productService;

    public ProductFacadeImpl(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public Result<ProductTO> create(ProductTO to) {
        var result = this.productService.create(to);
        return successful(result);
    }

    @Override
    public Result<ProductTO> load(Integer id) {
        var result = this.productService.get(id);
        return successful(result);
    }

    @Override
    public Result<ProductTO> update(ProductTO to) {
        var result = this.productService.update(to);
        return successful(result);
    }

    @Override
    public Result<ProductTO> delete(Integer id) {
        this.productService.delete(id);
        return successful();
    }

    @Override
    public Result<List<ProductTO>> search(ProductCriteriaTO criteria) {
        var all = this.productService.loadAll(criteria);
        return successful(all);
    }
}
