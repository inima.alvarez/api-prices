package com.acl.work.prices.apiprices.facade.impl;

import com.acl.work.prices.apiprices.core.facade.Result;
import com.acl.work.prices.apiprices.core.facade.stereotype.Facade;
import com.acl.work.prices.apiprices.core.security.JwtTokenProvider;
import com.acl.work.prices.apiprices.facade.AuthenticationFacade;
import com.acl.work.prices.apiprices.service.security.AuthenticationService;
import com.acl.work.prices.apiprices.service.security.contract.to.UserRequestTO;
import com.acl.work.prices.apiprices.service.security.contract.to.UserResponseTO;
import org.springframework.security.core.GrantedAuthority;

import static com.acl.work.prices.apiprices.core.facade.Result.failed;
import static com.acl.work.prices.apiprices.core.facade.Result.successful;
import static com.acl.work.prices.apiprices.core.facade.error.Error.err;
import static com.acl.work.prices.apiprices.core.facade.error.ErrorCode.FORBIDDEN_ACCESS;
import static java.util.stream.Collectors.toList;

@Facade("authenticationFacade")
public class AuthenticationFacadeImpl implements AuthenticationFacade {

    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationService authenticationService;

    public AuthenticationFacadeImpl(
            JwtTokenProvider jwtTokenProvider,
            AuthenticationService authenticationService
    ) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationService = authenticationService;
    }

    @Override
    public Result<UserResponseTO> logging(UserRequestTO request) {
        try {
            var token = this.retrieveToken(request);
            var response = new UserResponseTO().withToken(token);
            return successful(response);
        } catch (Exception e) {
            return failed(err(FORBIDDEN_ACCESS, e.getMessage()));
        }
    }

    private String retrieveToken(UserRequestTO request) {
        var logging = this.authenticationService.logging(request);
        var roles = logging
                .getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(toList());
        return this.jwtTokenProvider.generateJwtToken(logging.getUsername(), roles);
    }
}
