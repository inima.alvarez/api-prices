package com.acl.work.prices.apiprices.core.exceptions;

import com.acl.work.prices.apiprices.core.facade.error.ErrorCode;

import static com.acl.work.prices.apiprices.core.facade.error.ErrorCode.NOT_FOUND;

public class CustomNotFoundException extends BusinessException {

    private static final long serialVersionUID = 5760507686814975116L;

    public CustomNotFoundException() {
        super(NOT_FOUND);
    }

    public CustomNotFoundException(ErrorCode code) {
        super(code);
    }

    public CustomNotFoundException(ErrorCode code, Object v) {
        super(code, String.valueOf(v));
    }
}
