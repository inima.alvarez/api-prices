package com.acl.work.prices.apiprices.service.security.contract.to;

import com.acl.work.prices.apiprices.core.TransferObject;

public class UserResponseTO extends TransferObject {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserResponseTO withToken(String token) {
        this.setToken(token);
        return this;
    }
}
