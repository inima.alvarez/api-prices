package com.acl.work.prices.apiprices.facade.impl;

import com.acl.work.prices.apiprices.core.facade.Result;
import com.acl.work.prices.apiprices.core.facade.stereotype.Facade;
import com.acl.work.prices.apiprices.facade.PriceFacade;
import com.acl.work.prices.apiprices.service.prices.PriceService;
import com.acl.work.prices.apiprices.service.prices.contract.to.PageList;
import com.acl.work.prices.apiprices.service.prices.contract.to.PriceCriteriaTO;
import com.acl.work.prices.apiprices.service.prices.contract.to.PriceResponseTO;

import static com.acl.work.prices.apiprices.core.facade.Result.successful;

@Facade("priceFacade")
public class PriceFacadeImpl implements PriceFacade {
    private final PriceService priceService;

    public PriceFacadeImpl(PriceService priceService) {
        this.priceService = priceService;
    }

    @Override
    public Result<PageList<PriceResponseTO>> search(PriceCriteriaTO criteria) {
        var all = this.priceService.loadAll(criteria);
        return successful(all);
    }
}
