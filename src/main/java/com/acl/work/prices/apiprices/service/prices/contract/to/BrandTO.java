package com.acl.work.prices.apiprices.service.prices.contract.to;

import com.acl.work.prices.apiprices.core.TransferObject;

public class BrandTO extends TransferObject {
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BrandTO withId(Integer id) {
        this.setId(id);
        return this;
    }
}
