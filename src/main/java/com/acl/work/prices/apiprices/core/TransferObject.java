package com.acl.work.prices.apiprices.core;

import java.io.Serializable;

import static com.acl.work.prices.apiprices.util.json.JsonSerdes.jsonfy;

public class TransferObject implements Serializable {
    @Override
    public String toString() {
        return jsonfy(this);
    }
}
