# Getting Started

### Reference Documentation

For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.7.0/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.7.0/gradle-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.7.0/reference/htmlsingle/#web)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.7.0/reference/htmlsingle/#data.sql.jpa-and-spring-data)
* [Spring Security](https://docs.spring.io/spring-boot/docs/2.7.0/reference/htmlsingle/#web.security)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.7.0/reference/htmlsingle/#using.devtools)
* [Docker for Windows](https://docs.docker.com/desktop/windows/install/)
* [Docker Compose](https://docs.docker.com/compose/install/)

### Guides

The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Securing a Web Application](https://spring.io/guides/gs/securing-web/)
* [Spring Boot and OAuth2](https://spring.io/guides/tutorials/spring-boot-oauth2/)
* [Authenticating a User with LDAP](https://spring.io/guides/gs/authenticating-ldap/)

### Additional Links

These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

* [Platform](#platform)
* [Entry point](#entry-point)
* [Configuration Variables](#configuration-variables)

## Platform

- ***Spring Boot***: *2.7.0*
- ***Java***: *11*
- ***Gradle***: *7.4.1*
- ***PostgreSQL***: *13.1*
- ***Docker for Windows***: *17.05.0-ce*
- ***Docker compose***: *1.3.0*
- ***Postman***: *8.4.0*

## Entry point
The application entry point is the main class `Application` placed in the project root package.

## Configuration Variables
- ***SPRING_CONFIG_LOCATION***: Define the location of Spring Boot application by default take **optional:classpath:/application.properties**
- ***DEFAULT_LOGGING_LEVEL***: Define the log level of Spring Boot by default is **INFO**
- ***DATASOURCE_USERNAME***: Define the PostgreSQL username
- ***DATASOURCE_PASSWORD***: Define the PostgreSQL password
- ***DATASOURCE_HOST***: Define the PostgreSQL host
- ***DATASOURCE_PORT***: Define the PostgreSQL port
- ***DATASOURCE_INSTANCE***: Define the PostgreSQL database name to be use in the application
- ***DATASOURCE_SCHEMA***: Define the PostgreSQL schema to be use


## How it's works
- Path: **/prices/v1**
- Port: **8081**

### Run the application
This application run over docker compose, using the latest image of **docker-spring-boot-postgres** and the version **13.1-alpine** of postgres.
To run the application execute the below command:
 - For Windows platform
    ```shell
        execute.cmd
    ```
 - For Linux or Mac platform
   ```shell
        ./execute.sh
    ```
   
### Test the application with Postman
In the root directory of the application you will find the Postman collection and environment variables which you can import into Postman for functionality testing.

- [How to import Postman collection](https://learning.postman.com/docs/getting-started/importing-and-exporting-data/)
- [How to import Postman environment variables](https://testfully.io/blog/import-from-postman/#import-postman-environment-to-testfully)
  
   - **Postman Collection**
     PRICES.postman_collection.json
   - **Postman environment**
     PRICES.postman_environment.json

### Api documentation
The API documentation and description of the endpoints is available from Swagger and is accessible at: [Documentation](http://localhost:8081/prices/v1/swagger-ui/index.html#/)





