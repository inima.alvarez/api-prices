FROM adoptopenjdk:11-jre-hotspot
ARG JAR_FILE=build/libs/api-prices-0.0.1.jar
COPY ${JAR_FILE} application.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "application.jar"]